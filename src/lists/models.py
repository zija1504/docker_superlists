from django.db.models.deletion import CASCADE
from django.db import models
from django.urls import reverse
from django.db.models.constraints import UniqueConstraint

# Create your models here.


class List(models.Model):
    def get_absolute_url(self):
        return reverse("view_list", args=[self.pk])


class Item(models.Model):
    text = models.TextField(default="")
    list = models.ForeignKey(List, default="", on_delete=models.CASCADE)

    class Meta:
        constraints = [
            UniqueConstraint(fields=["text", "list"], name="unique_item"),
        ]
        ordering = [
            "id",
        ]

    def __str__(self):
        return self.text


class Item1(models.Model):
    text = models.TextField(default="")
    list = models.ForeignKey(List, default="", on_delete=models.CASCADE)
