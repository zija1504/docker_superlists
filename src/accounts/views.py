from accounts.models import Token
from django.http.request import HttpRequest, QueryDict
from django.shortcuts import render, redirect
from django.core.mail import send_mail
from django.contrib import messages, auth
from django.urls import reverse
import sys


def send_login_email(request: HttpRequest):
    email = request.POST.get("email", "")
    token = Token.objects.create(email=email)
    url = request.build_absolute_uri(reverse("login") + "?token=" + str(token.uid))
    message_body = f"Use this link to log in:\n\n{url}"
    send_mail("Your login link for Superlists", message_body, "noreply@superlists", [email])
    messages.success(request, "Check your email, we've sent you a link you can use to log in.")
    return redirect("/")


def login(request: HttpRequest):
    user = auth.authenticate(request=request, uid=request.GET.get("token"))
    if user:
        print("udało się", file=sys.stderr)
        auth.login(request, user)
    else:
        print("błąd", file=sys.stderr)
    return redirect("/")


def logout(request: HttpRequest):
    auth.logout(request)
    return redirect("/")
