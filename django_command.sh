#!/bin/sh

docker-compose -f docker-compose.ci-prod.yml exec web  src/manage.py migrate --no-input
docker-compose -f docker-compose.ci-prod.yml down
docker-compose -f docker-compose.ci-prod.yml up -d
