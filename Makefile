PHONY: docs clean

COMMAND = docker-compose exec  web /bin/sh -c

all: build run test

build:
	docker-compose build

run:
	docker-compose up -d

migrate:
	$(COMMAND) './src/manage.py migrate'

collectstatic:
	docker-compose run --rm web src/manage.py collectstatic --no-input


test:
	$(COMMAND) "pytest"


coverage:
	$(COMMAND) "coverage report"

clean:
	rm -rf build
	rm -rf hello.egg-info
	rm -rf dist
	rm -rf htmlcov
	rm -rf .tox
	rm -rf .cache
	rm -rf .pytest_cache
	find . -type f -name "*.pyc" -delete
	rm -rf $(find . -type d -name __pycache__)
	rm .coverage
	rm .coverage.*

dockerclean:
	docker system prune -f
	docker system prune -f --volumes

build_prod:
	docker-compose  down -v --remove-orphans
	docker-compose -f docker-compose.prod.yml up -d --build

.ONESHELL:
ci_test:
	TAG=$(shell date +TEST-%F-%H-%M);\
	git tag $$TAG; \
	git push origin $$TAG;

.ONESHELL:
ci_stage:
	TAG=$(shell date +STAGING-%F-%H-%M);\
	git tag $$TAG; \
	git push origin $$TAG;

.ONESHELL:
ci_deploy:
	TAG=$(shell date +DEPLOY-%F-%H-%M);\
	git tag $$TAG; \
	git push origin $$TAG;

