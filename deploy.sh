#!/bin/sh

ssh -o StrictHostKeyChecking=no root@$DIGITAL_OCEAN_IP_ADDRESS  << 'ENDSSH'
cd /app
export $(cat .env | xargs)
docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
docker pull $IMAGE:web-staging
docker pull $IMAGE:nginx-staging
docker-compose -f docker-compose.ci-prod.yml up -d --build
docker-compose -f docker-compose.ci-prod.yml exec  -T web src/manage.py migrate --no-input
ENDSSH
