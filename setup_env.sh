#!/bin/sh

echo DEBUG=0 >> .env
echo SQL_ENGINE=django.db.backends.postgresql >> .env
echo DATABASE=postgres >> .env
echo ALLOWED_HOSTS=$DIGITAL_OCEAN_IP_ADDRESS zija1504.edu.pl localhost 127.0.0.1 >> .env
echo SECRET_KEY=$SECRET_KEY >> .env
echo SQL_DATABASE=$SQL_DATABASE >> .env
echo SQL_USER=$SQL_USER >> .env
echo SQL_PASSWORD=$SQL_PASSWORD >> .env
echo SQL_HOST=$SQL_HOST >> .env
echo SQL_PORT=$SQL_PORT >> .env
echo WEB_IMAGE=$WEB_IMAGE  >> .env
echo NGINX_IMAGE=$NGINX_IMAGE >> .env
echo CI_REGISTRY_USER=$CI_REGISTRY_USER   >> .env
echo CI_JOB_TOKEN=$CI_JOB_TOKEN  >> .env
echo CI_REGISTRY=$CI_REGISTRY  >> .env
echo IMAGE=$CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME >> .env
echo POSTGRES_DB=$SQL_DATABASE >> .env
echo POSTGRES_USER=$SQL_USER >> .env
echo POSTGRES_PASSWORD=$SQL_PASSWORD >> .env
echo POSTRGRES_HOST=$SQL_HOST >> .env
echo POSTRGRES_PORT=$SQL_PORT >> .env
echo EMAIL_PASSWORD=$EMAIL_PASSWORD >> .env
