from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from .base import Functional


class NewVisitorTest(Functional):
    def test_can_start_list_and_retrieve_later(self) -> None:
        # user open browser and check out new web page
        self.browser.get(self.live_server_url)
        # user notices the page title and header mention to-do lists
        assert "To-Do" in self.browser.title
        header_text: str = self.browser.find_element_by_tag_name("h1").text
        assert "To-Do" in header_text
        # user can enter a to-do item straight away
        inputbox = self.get_item_input_box()
        assert inputbox.get_attribute("placeholder") == "Enter a to-do item"

        # user types "Buy milk and sugar"
        inputbox.send_keys("Buy a milk")
        #  when user hits enter, the page updates, and now:
        # "1: Buy milk and sugar"
        inputbox.send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table("1: Buy a milk")
        # there is still text box for next item
        # user writes Cook pancakes for dinner"
        inputbox1 = self.get_item_input_box()
        inputbox1.send_keys("Cook pancakes for dinner")
        inputbox1.send_keys(Keys.ENTER)
        # page updates again
        self.wait_for_row_in_list_table("1: Buy a milk")
        self.wait_for_row_in_list_table("2: Cook pancakes for dinner")
        # user sees unique url for that list

    def test_multiple_users_can_start_lists_at_diffrent_urls(self):
        # Edith start a new to-do lists
        self.browser.get(self.live_server_url)
        inputbox = self.get_item_input_box()
        inputbox.send_keys("Buy a milk")
        inputbox.send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table("1: Buy a milk")

        # she notices that her list has unique url
        edith_list_url: str = self.browser.current_url
        self.assertRegex(edith_list_url, "/lists/.+")
        # new user, Francis, comes along to the site

        # Francis visit the home page, no sign of Edith's list
        self.browser.get(self.live_server_url)
        page_text: str = self.browser.find_element_by_tag_name("body").text
        self.assertNotIn("Buy a milk", page_text)
        self.assertNotIn("Cook pankaces for dinner", page_text)
