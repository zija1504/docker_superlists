
from typing import Collection, Optional, Union
import pytest
from django.db.models.base import Model
from django.http import HttpRequest
from django.http.response import HttpResponse
from django.test import TestCase
from django.test.client import Client
from django.urls import resolve
from django.urls.resolvers import ResolverMatch
from django.utils.html import escape

from django.test.client import Client
from lists.forms import ItemForm
import pytest


from lists.forms import (
    _DUPLICATE_ITEM_ERROR,
    _EMPTY_ITEM_ERROR,
    ExistingListItemForm,
    ItemForm,
)
from lists.models import Item, List
from lists.views import home_page


class TestHomePage:
    def test_home_page_returns_correct_html(self, client):
        response: HttpResponse = client.get("/")
        assert "home.html" in (t.name for t in response.templates)

    def test_home_page_uses_item_form(self, client: Client):
        response: HttpResponse = client.get("/")
        assert isinstance(response.context["form"], ItemForm)


class TestListView:
    @pytest.mark.django_db
    def test_displays_only_items_for_that_list(self, client):
        correct_list = List.objects.create()
        Item.objects.create(text="itemey 1", list=correct_list)
        Item.objects.create(text="itemey 2", list=correct_list)
        other_list = List.objects.create()
        Item.objects.create(text="other list item 1", list=other_list)
        Item.objects.create(text="other list item 2", list=other_list)

        response: HttpResponse = client.get(f"/lists/{correct_list.pk}/")

        assert "itemey 1" in response.content.decode("utf-8")
        assert "itemey 2" in response.content.decode("utf-8")
        assert "other list item 1" not in response.content.decode("utf-8")
        assert "other list item 2" not in response.content.decode("utf-8")

    @pytest.mark.django_db
    def test_uses_list_templates(self, client):
        list_ = List.objects.create()
        response: HttpResponse = client.get(f"/lists/{list_.pk}/")
        assert "list.html" in (t.name for t in response.templates)

    @pytest.mark.django_db
    def test_passes_correct_list_to_template(self, client):
        other_list = List.objects.create()  # noqa
        correct_list = List.objects.create()
        response: HttpResponse = client.get(f"/lists/{correct_list.pk}/")
        assert response.context["list"] == correct_list

    @pytest.mark.django_db
    def test_can_save_a_POST_request_to_an_existing_list(self, client):
        other_list = List.objects.create()  # noqa
        correct_list = List.objects.create()

        client.post(
            f"/lists/{correct_list.pk}/",
            data={"text": "A new item for an existing list"},
        )

        assert Item.objects.count() == 1
        new_item: Optional[Item] = Item.objects.first()
        if new_item:
            assert new_item.text == "A new item for an existing list"
            assert new_item.list == correct_list

    @pytest.mark.django_db
    def test_redirects_to_list_view(self, client):
        other_list = List.objects.create()  # noqa
        correct_list = List.objects.create()
        response: HttpResponse = client.post(
            f"/lists/{correct_list.pk}/",
            data={"text": "A new item for an existing list"},
        )
        assert response.status_code == 302
        assert response.url == f"/lists/{correct_list.pk}/"

    def post_invalid_input(self, client: Client) -> HttpResponse:
        list_ = List.objects.create()
        return client.post(f"/lists/{list_.pk}/", data={"text": ""})

    @pytest.mark.django_db
    def test_for_invalid_input_shows_error_on_lists_page(self, client: Client):
        response = self.post_invalid_input(client)
        assert escape(_EMPTY_ITEM_ERROR) in response.content.decode("utf-8")

    @pytest.mark.django_db
    def test_for_invalid_input_passes_form_to_the_template(self, client: Client):
        response = self.post_invalid_input(client)
        assert isinstance(response.context["form"], ExistingListItemForm)

    @pytest.mark.django_db
    def test_for_invalid_input_renders_list_template(self, client: Client):
        response = self.post_invalid_input(client)
        list_ = List.objects.create()
        response: HttpResponse = client.post(
            f"/lists/{list_.pk}/", data={"item_text": ""}
        )

        assert response.status_code == 200
        assert "list.html" in (t.name for t in response.templates)

    @pytest.mark.django_db
    def test_for_invalid_input_nothing_saved_to_db(self, client):
        response = self.post_invalid_input(client)
        assert Item.objects.count() == 0

    @pytest.mark.django_db
    def test_displays_item_form(self, client: Client):
        list_ = List.objects.create()
        response: HttpResponse = client.get(f"/lists/{list_.pk}/")
        assert isinstance(response.context["form"], ExistingListItemForm)
        assert 'name="text"' in response.content.decode("utf-8")

    @pytest.mark.django_db
    def test_duplicate_item_validation_errors_end_up_on_list_page(self, client: Client):
        list1 = List.objects.create()
        item1 = Item.objects.create(list=list1, text="textey")
        response: HttpResponse = client.post(
            f"/lists/{list1.id}/", data={"text": "textey"}
        )
        expected_error = escape(_DUPLICATE_ITEM_ERROR)
        assert expected_error in response.content.decode("utf-8")
        assert "list.html" in (t.name for t in response.templates)
        assert Item.objects.all().count() == 1


class TestNewListView:
    @pytest.mark.django_db
    def test_can_save_a_POST_request(self, client):
        client.post("/lists/new", data={"text": "A new list item"})
        assert Item.objects.count() == 1
        new_item = Item.objects.first()
        assert new_item.text == "A new list item"

    @pytest.mark.django_db
    def test_redirect_after_POST(self, client):
        response: HttpResponse = client.post(
            "/lists/new", data={"text": "A new list item"}
        )
        new_list = List.objects.first()
        assert response.status_code == 302
        assert response.url == f"/lists/{new_list.pk}/"

    @pytest.mark.django_db
    def test_for_invalid_input_renders_home_page_template(self, client):
        response: HttpResponse = client.post("/lists/new", data={"text": ""})

    def test_validation_errors_are_sent_back_to_home_page_template(self, client):
        response: HttpResponse = client.post("/lists/new", data={"item_text": ""})

        assert response.status_code == 200
        assert "home.html" in (t.name for t in response.templates)

    @pytest.mark.django_db
    def test_validation_errors_are_shown_on_home_page(self, client):
        response: HttpResponse = client.post("/lists/new", data={"text": ""})
        assert escape(_EMPTY_ITEM_ERROR) in response.content.decode("utf-8")

    @pytest.mark.django_db
    def test_for_invalid_input_passes_form_to_template(self, client):
        response: HttpResponse = client.post("/lists/new", data={"text": ""})
        assert isinstance(response.context["form"], ItemForm)

    @pytest.mark.django_db
    def test_invalid_list_items_arent_saved(self, client):
        client.post("/lists/new", data={"text": ""})
        assert List.objects.count() == 0
        assert Item.objects.count() == 0


class TestMyLists:
    def test_my_lists_url_renders_my_lists_template(self, client: Client):
        response = client.get('/lists/users/a@b.com/')
        assert response.status_code == 200
        assert "my_lists.html" in (t.name for t in response.templates)
