from typing import List

import pytest
from django.contrib.messages.storage.base import Message
from django.http.response import HttpResponse
from django.test.client import Client

import accounts.views
from accounts.models import Token


@pytest.mark.django_db
class TestSendLoginEmailViewTest:
    def test_redirects_to_home_page(self, client: Client):
        response: HttpResponse = client.post(
            "/accounts/send_login_email", data={"email": "edith@example.com"}
        )
        assert response.status_code == 302
        assert response.url == "/"

    def test_sends_mail_to_address_from_post(self, client: Client, mocker):
        self.send_mail_called = False

        mock_send_mail = mocker.patch.object(accounts.views, "send_mail")
        client.post("/accounts/send_login_email", data={"email": "edith@example.com"})
        assert mock_send_mail.called is True
        (subject, body, from_email, to_list), kwargs = mock_send_mail.call_args

        assert subject == "Your login link for Superlists"
        assert from_email == "noreply@superlists"
        assert to_list == ["edith@example.com"]

    def test_adds_success_message(self, client: Client):
        response: HttpResponse = client.post(
            "/accounts/send_login_email", data={"email": "edith@example.com"}, follow=True
        )

        message: Message = list(response.context["messages"])[0]
        assert message.message == "Check your email, we've sent you a link you can use to log in."
        assert message.tags == "success"

    @pytest.mark.django_db
    def test_creates_token_associated_with_email(self, client: Client):
        client.post("/accounts/send_login_email", data={"email": "edith@example.com"})
        token = Token.objects.first()
        assert token.email == "edith@example.com"

    @pytest.mark.django_db
    def test_sends_link_to_login_using_token_uid(self, client, mocker):
        mock_send_mail = mocker.patch.object(accounts.views, "send_mail")
        client.post("/accounts/send_login_email", data={"email": "edith@example.com"})

        token = Token.objects.first()
        expected_url = f"http://testserver/accounts/login?token={token.uid}"
        (subject, body, from_email, to_list), kwargs = mock_send_mail.call_args
        assert expected_url in body


@pytest.mark.django_db
class TestLoginView:
    def test_redirects_to_home_page(self, client: Client):
        response: HttpResponse = client.get("/accounts/login?token=abcd123")
        assert response.status_code == 302
        assert response.url == "/"

    def test_calls_authenticate_with_uid_from_get_request(self, client, mocker):
        mock_auth = mocker.patch.object(accounts.views, "auth")
        response = client.get("/accounts/login?token=abcd123")
        assert mock_auth.authenticate.call_args == mocker.call(
            request=response.wsgi_request, uid="abcd123"
        )

    def test_calls_auth_login_with_user_if_there_is_one(self, client, mocker):
        mock_auth = mocker.patch.object(accounts.views, "auth")
        response = client.get("/accounts/login?token=abcd123")
        assert mock_auth.login.call_args == mocker.call(
            response.wsgi_request, mock_auth.authenticate.return_value
        )

    def test_does_not_login_if_user_is_not_authenticated(self, client, mocker):
        mock_auth = mocker.patch.object(accounts.views, "auth")
        mock_auth.authenticate.return_value = None
        client.get("/accounts/login?token=abcd123")
        assert mock_auth.login.called is False
