import pytest
from django.contrib import auth

from accounts.models import Token

User = auth.get_user_model()
# auth.signals.user_logged_in.disconnect(auth.models.update_last_login)


@pytest.mark.django_db
class TestUserModel:
    def test_user_is_vaild_with_email_only(self):
        user = User(email="a@b.com")
        user.full_clean()

    def test_email_is_primary_key(self):
        user = User(email="a@b.com")
        assert user.pk == "a@b.com"

    def test_no_problem_with_auth_login(self, client):
        user = User.objects.create(email="edith@example.com")
        user.backend = ""
        request = client.request().wsgi_request
        auth.login(request, user)  # should not raise


@pytest.mark.django_db
class TestTokenModel:
    def test_links_user_with_auto_generated_uid(self):
        token1 = Token.objects.create(email="a@b.com")
        token2 = Token.objects.create(email="a@b.com")
        assert token1.uid != token2.uid
